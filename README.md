Description says it all, but I'll say it again, its an app that converts
between base systems.

So, if you want the basic rundown of how it works, when the user enters
something into the text field and presses one of the radio buttons, the app
takes this value, parses out only the relevant characters, then uses Java's
parseInt method to convert that number to decimal.

Next, when the user presses a radio button at the bottom, the app converts from
decimal to the chosen base by peforming a number of division / modulo operations

It's still a work in progress, but shows I know my way around eclipse and
with android development.

It's free for anyone to use.  In the future, I plan on expanding it by allowing
the user to perform two's compliment conversions and arithmetic, making it
a much handy tool for anyone who needs a quick calculator for alternative bases.

I've been told I should charge for the latter, but I'm still debating it.
I'd much rather build a fan base first, then charge for awesome games and neat
tablet-based robots.
