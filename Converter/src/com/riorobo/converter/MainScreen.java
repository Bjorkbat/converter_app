// Author: John Borden
//
// There is no license for the moment, not terribly interested in adding one at the moment
// If I do add one, it'll probably be an MIT or BSD open source license.
//
// So, do I just add it up there, or what?
//
// Anyway, this is a simple personal project of mine with practical applications
// It takes a number in some base system, and tells you what it is converted to another base system.
// It could use improvement, and it will receive it, in due time.
// Feel free to modify for your own use.

package com.riorobo.converter;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RadioGroup;
import android.widget.RadioButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Button;

public class MainScreen extends Activity {
	
	// The set of radio buttons responsible for selecting the base of the input
	// in case you're wondering, creating a button for hexadecimal input isn't necessary
	// If none of the other values are selected when the check listener is detects a check
	// Then the program assumes that the hexadecimal button was checked
	private RadioGroup inRadio;
	
	private RadioButton binary_input;
	private RadioButton octal_input;
	private RadioButton decimal_input;
	
	// The set of radio buttons responsible for selecting the base of the output
	private RadioGroup outRadio;
	
	private RadioButton binary_output;
	private RadioButton octal_output;
	private RadioButton decimal_output;
	
	//A simple clear button, to clear button selection and fields
	private Button clearChecked;
	
	// Input and output fields, the former being an editable field, the latter being
	// a simple text field
	private EditText input_prompt;
	private TextView output;
	
	// A little less obvious, the raw_input is whatever input is received by
	// the edit text field.
	// The converted output is what gets set to the textview
	private String raw_input;
	private String converted_output = "";
	
	// Meanwhile, the num_input variable is the raw_input converted
	// to an appropriate integer value
	// The num_mutable variable is used for conversion, and was defined
	// Because it is eaten (i.e. gradually whittled down to zero) in the process
	private int num_input;
	private int num_mutable;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_screen);
		
		// First things first, take all the views in the layout and assign them
		// to their respective variables in Java
		inRadio = (RadioGroup)findViewById(R.id.input_selector);
		
		binary_input = (RadioButton)findViewById(R.id.input_binary);
		octal_input = (RadioButton)findViewById(R.id.input_octal);
		decimal_input = (RadioButton)findViewById(R.id.input_decimal);
		
		outRadio = (RadioGroup)findViewById(R.id.output_selector);
		
		binary_output = (RadioButton)findViewById(R.id.output_binary);
		octal_output = (RadioButton)findViewById(R.id.output_octal);
		decimal_output = (RadioButton)findViewById(R.id.output_decimal);
		
		clearChecked = (Button) findViewById(R.id.clearRadio);
		
		input_prompt = (EditText)findViewById(R.id.input_prompt);
		output = (TextView)findViewById(R.id.output_prompt);
		
		// Then, go about setting a listener for the inRadio radio group
		inRadio.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				
				// Very straight forward, the entry has all unnecessary characters
				// removed, gets converted to a decimal number, and is
				// assigned to the num_input variable
				if(binary_input.isChecked()) {
					
					raw_input = input_prompt.getText().toString();
					num_input = MainScreen.textToInt(raw_input, 2);
				}
				else if(octal_input.isChecked()) {
					
					raw_input = input_prompt.getText().toString();
					num_input = MainScreen.textToInt(raw_input, 8);
				}
				else if(decimal_input.isChecked()) {
					
					raw_input = input_prompt.getText().toString();
					num_input = MainScreen.textToInt(raw_input, 10);
				}
				else {
					raw_input = input_prompt.getText().toString();
					num_input = MainScreen.textToInt(raw_input, 16);
				}
				// The mechanics of the textToInt method are explained further below
			}
		});
		
		// Here another listener is created, this time for the outRadio radio group
		outRadio.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				
				// A safeguard against any early button presses
				// Or the possibility that the user actually entered a zero
				// Otherwise, the means of conversion outlined below
				// Are not intended for zeros
				if(num_input == 0) {
					output.setText("0");
				}
				else if(binary_output.isChecked()) {
					
					converted_output = "";
					num_mutable = num_input;
					
					// If the user wants the answer in binary, then the program simply calculates
					// the modulo and uses that to determine what to add to the output
					while(num_mutable > 0) {
						
						converted_output = (num_mutable % 2) + converted_output;
						
						//For every step,  whittle down num_mutable
						num_mutable = num_mutable / 2;
					}
					
					output.setText(converted_output);
				}
				else if(octal_output.isChecked()) {
					
					converted_output = "";
					num_mutable = num_input;
					
					// Same as before, except this time, simply add to output
					// the result of the modulus operation
					while(num_mutable > 0) {
						
						converted_output = (num_mutable % 8) + converted_output;
						
						num_mutable = num_mutable / 8;
					}
					
					output.setText(converted_output);
				}
				else if(decimal_output.isChecked()) {
					
					// Because of the way the textToInt method works
					// A conversion isn't even necessary
					converted_output = "" + num_input;
					output.setText(converted_output);
				}
				else {
					
					converted_output = "";
					num_mutable = num_input;
					
					// Hexadecimal is more tricky, since one cannot simply calculate the modulo
					// of a character by an integer.
					// Actually, there probably is a way, and it's probably pretty elegant
					// But this will likely do for the time being
					// At least it shows I don't have an irrational fear of switch statements
					while(num_mutable > 0) {
						
						// So, if the modulo happens to be greater than 9, then  use the switch below
						// To determine just what exactly the output should be set to
						if(num_mutable % 16 > 9) {
							int modulus = num_mutable % 16;
							switch (modulus) {
							
							case 10:
								converted_output = "A" + converted_output;
								break;
							case 11:
								converted_output = "B" + converted_output;
								break;
							case 12:
								converted_output = "C" + converted_output;
								break;
							case 13:
								converted_output = "D" + converted_output;
								break;
							case 14:
								converted_output = "E" + converted_output;
								break;
							case 15:
								converted_output = "F" + converted_output;
								break;
							default:
								converted_output = "G" + converted_output;
								break;
							}
						}
						// If the modulo is less than or equal to nine, then do as before
						// and simply add the modulo to the output
						else 
							converted_output = (num_mutable % 16) + converted_output;
						
						num_mutable = num_mutable / 16;
					}
					
					output.setText(converted_output);
				}
			}
		});
		
		//Pretty straight forward, if you click the clear button, well, you clear everything
		clearChecked.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				inRadio.clearCheck();
				outRadio.clearCheck();
				input_prompt.setText("");
				output.setText("Output will be displayed here");
			}
		});
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main_screen, menu);
		return true;
	}
	
	// This is the method used to convert the input text into an integer
	// pretty straight forward, you feed it your input plus the base it was in
	// For output, you get a decimal representation of that number
	private static int textToInt(String raw_input, int base) {
		
		// For our variables, there's parsed input, used to represent input
		// without any unnecessary characters, and answer, which is just the
		// return value
		String parsed_input = "";
		int answer = 0;
		
		//If there's nothing in input, don't even bother
		if(raw_input.length() == 0)
			return 0;
		
		//Once again, another switch statement, the base determining how the code behaves
		switch (base) {
		
		case 2:
			
			// Pretty straightforward, only add numbers that are 1 or 0 to the
			// parsed_input String variable
			for(int i = 0; i < raw_input.length(); i++) {
				
				if(raw_input.charAt(i) == '1' || raw_input.charAt(i) == '0')
					parsed_input = parsed_input + raw_input.charAt(i);
			}
			
			// Then, use Java's parseInt method to convert it from binary to decimal
			// No need to reinvent the wheel here
			if(!parsed_input.equals(""))
				answer = Integer.parseInt(parsed_input, 2);
			break;
			
		case 8:
			
			// No different from what you saw before, only this time we're looking for
			// any character that happens to be a digit and is less than 8
			for(int i = 0; i < raw_input.length(); i++) {
				
				if(Character.isDigit(raw_input.charAt(i)) && raw_input.charAt(i) < '8')
					parsed_input = parsed_input + raw_input.charAt(i);
			}
			
			if(!parsed_input.equals(""))
				answer = Integer.parseInt(parsed_input, 8);
			break;
			
		case 10:
			
			// Handling decimal values is even easier than octals
			// This time, we just look for any character that happens to be a digit
			for(int i = 0; i < raw_input.length(); i++) {
				
				if(Character.isDigit(raw_input.charAt(i)))
					parsed_input = parsed_input + raw_input.charAt(i);
			}
			
			if(!parsed_input.equals(""))
				answer = Integer.parseInt(parsed_input);
			break;
			
		case 16:
			
			// Hexadecimal values are one of the trickier numbers to handle
			// First, let's take raw_input and convert all the characters to Upper Case...
			raw_input = raw_input.toUpperCase();
			
			// ... Then, we select characters that happen to be digits or
			// characters that are appropriate for use in hexadecimal
			for(int i = 0; i < raw_input.length(); i ++) {
				
				// For sake of readability, the program looks for digit values first
				if(Character.isDigit(raw_input.charAt(i)))
					parsed_input = parsed_input + raw_input.charAt(i);
				// If the character isn't a digit, then it checks to see if it might be
				// an appropriate hexadecimal letter
				else if(raw_input.charAt(i) <= 'F' && raw_input.charAt(i) >= 'A')
					parsed_input = parsed_input + raw_input.charAt(i);
			}
			
			if(!parsed_input.equals(""))
				answer = Integer.parseInt(parsed_input, 16);
			break;
			
		default:
			
			// The program cannot display negative values
			// Thus, if there's an error, the answer is negative one
			// Hopefully the end user won't see this pop up
			answer = -1;
			break;
		}
		
		return answer;
	}

}
